/*global window:false */
'use strict';

module.exports = function(path) {
  var hostname   = window.location.host || '';
  var protocol   = window.location.protocol || 'http:';
  var hostParts  = hostname.split(':');
  var hostPort   = hostParts.length > 1 ? parseInt(hostParts.pop()) : 80;
  var hostDomain = hostParts.join(':');

  var urlLink = protocol + '//' + hostDomain + (hostPort === 80 ? '' : ':'.concat(hostPort)) + '/' + path;
  return urlLink;
};
