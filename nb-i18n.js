'use strict';

var i18n = {};
var degaultLang = 'pt-br';

Object.prototype.setTranslate = function(lang) {
  if (!lang) lang = degaultLang;
  if (!i18n[lang]) i18n[lang] = {};

  Object.keys(this).forEach(function(key) {
    i18n[lang][key] = this[key];
  });
};

String.prototype.setDefaultLang = function() {
  degaultLang = this;
};

String.prototype.translate = function() {
  var key = this;

  var withoutI18N = !Boolean(i18n[degaultLang] && i18n[degaultLang].hasOwnProperty(key) && i18n[degaultLang][key]);
  if (withoutI18N) return key;

  var translate = i18n[degaultLang][key];

  if (arguments) {
    for(var arg in arguments) {
      var val = arguments[arg];
      var argReplace = new RegExp('\\{' + String(arg) + '\\}', 'g');
      translate = String(translate).replace(argReplace, val);
    }
  }

  return translate;
};
